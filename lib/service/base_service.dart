import 'dart:convert';

import 'package:admin_app/errors/base_exception.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';

class BaseService {

  late Dio dio;

  BaseService() {
    dio = GetIt.I<Dio>();
  }


  void validate(Response response) {
    if (response.data['status'] != 'OK') {
      throw BaseException(response.data['msg']);
    }
  }

  String decode(String data) {
    Codec<String, String> stringToBase64Url = utf8.fuse(base64Url);
    return stringToBase64Url.decode(data);
  }

}