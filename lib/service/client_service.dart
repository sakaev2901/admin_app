import 'package:admin_app/service/base_service.dart';
import 'package:dio/dio.dart';
import 'package:hive/hive.dart';

class ClientService extends BaseService {
  
  Future<Response> fetchClients() async {
    var box = await Hive.openBox('box');
    String token = await box.get('token');
    var response = await dio.post('/get-client-not-confirmation', data: {
      'sessionId': token,
    });
    validate(response);
    return response;
  }

  Future<Response> fetchSingleClient(int id) async {
    var box = await Hive.openBox('box');
    String token = await box.get('token');
    var response = await dio.post('/get-client-for-mobile', data: {
      'sessionId': token,
      "client_id": id.toString(),
    });
    validate(response);
    return response;
  }

  Future<void> validateClient(int id) async {
    var box = await Hive.openBox('box');
    String token = await box.get('token');
    var response = await dio.post('/change-client-confirmation', data: {
      'sessionId': token,
      "clientId": id.toString(),
    });
    validate(response);
  }
}