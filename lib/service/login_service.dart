import 'dart:convert';

import 'package:admin_app/service/base_service.dart';
import 'package:dio/dio.dart';

class LoginService extends BaseService{

  Future<String> login(String phone, String password) async {
    var response = await dio.post('/auth', data: {
      'phone': phone,
      'password': password,
    });
    print(response.data);
    validate(response);
    return response.data['session_id'];
  }
}