import 'package:admin_app/icon/custom_icons_icons.dart';
import 'package:admin_app/widgets/app_title.dart';
import 'package:admin_app/widgets/base_page.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BasePage(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(child: AppTitle(title: 'Ильназ Ибрагимов')),
              SizedBox(width: 70,),
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Icon(CustomIcons.logout, size: 20, color: Color(0xFFBEC1CC),),
              ),
            ],
          ),
          SizedBox(height: 5,),
          Text('Администратор • Владивосток', style: TextStyle(color: Color(0xFF979CA9)),),
          SizedBox(height: 28,),
        ],
      ),
      selectedPage: 4,
    );
  }
}
