import 'package:admin_app/bloc/login/login_bloc.dart';
import 'package:admin_app/extensions/color_scheme_extension.dart';
import 'package:admin_app/page/add_bonuses/add_bonuses_page.dart';
import 'package:admin_app/provider/login_provider.dart';
import 'package:admin_app/service/login_service.dart';
import 'package:admin_app/widgets/base_page.dart';
import 'package:admin_app/widgets/input/custom_text_input.dart';
import 'package:admin_app/widgets/input/input_type.dart';
import 'package:admin_app/widgets/main_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:easy_padding/easy_padding.dart';

import '../access_recovery/access_recovery_screen.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    BlocProvider.of<LoginBloc>(context).stream.listen((event) {
      if (event is LoginSuccessState) {
        Navigator.of(context)
            .push(CupertinoPageRoute(builder: (_) => AddBonusesPage()));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return BasePage(
      child: Container(
        width: double.infinity,
        child: Column(
          children: [
            const SizedBox(
              height: 64,
            ),
            SvgPicture.asset(
              'assets/logo.svg',
              width: MediaQuery.of(context).size.width * 0.6,
            ),
            const SizedBox(
              height: 64,
            ),
            const Text(
              'Вход',
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 28,
            ),
            BlocBuilder<LoginBloc, LoginState>(
              builder: (context, state) {
                if (state is LoginFailState) {
                  return Text(
                    state.error,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.red,
                    ),
                  ).only(bottom: 10);
                }
                return SizedBox.shrink();
              },
            ),
            CustomTextInput(
              label: 'Номер телефона',
              hint: '7xxxxxxxxxx',
              inputType: InputType.PHONE,
              controller: _phoneController,
            ),
            const SizedBox(
              height: 20,
            ),
            CustomTextInput(
              label: 'Ваш пароль',
              hint: '•••••••••••••',
              inputType: InputType.PASSWORD,
              controller: _passwordController,
            ),
            SizedBox(
              height: 16,
            ),
            GestureDetector(
              onTap: () => Navigator.of(context).push(
                  CupertinoPageRoute(builder: (_) => AccessRecoveryScreen())),
              child: Text(
                'Забыли пароль?',
                style: TextStyle(
                  fontSize: 12,
                  color: Theme.of(context).colorScheme.chipTextColor,
                  decoration: TextDecoration.underline,
                ),
              ),
            ),
            SizedBox(
              height: 18,
            ),
            // MainButton(text: 'Войти', onPressed: () => Navigator.of(context).push(CupertinoPageRoute(builder: (_) => AddBonusesPage())),)
            MainButton(
              text: "Войти",
              onPressed: () {
                BlocProvider.of<LoginBloc>(context).add(LoginButtonPressed(
                    _phoneController.text, _passwordController.text));
              },
            )
          ],
        ),
      ),
      selectedPage: -1,
      showBottomBar: false,
    );
  }
}
