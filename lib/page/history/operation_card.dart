import 'package:admin_app/icon/custom_icons_icons.dart';
import 'package:admin_app/page/history/operation_type.dart';
import 'package:flutter/material.dart';

class OperationCard extends StatelessWidget {
  final OperationType type;
  int? sum;

  OperationCard({Key? key, required this.type, this.sum}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10,),
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 14,
          vertical: 14,
        ),
        decoration: BoxDecoration(
          color: Color(0xFFF1F1F1),
          borderRadius: BorderRadius.circular(12),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${_getType()}  •  15 марта 2022  •  17:54',
                    style: TextStyle(fontSize: 12, color: Color(0xFF979CA9)),
                  ),
                  SizedBox(
                    height: 6,
                  ),
                  Text(
                    'Константин Ивонин • Bronze > 5%',
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  SizedBox(
                    height: 13,
                  ),
                  Row(
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: _getColor(),
                        ),
                        child: Text(_getMessage(), style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.white),),
                      ),
                      SizedBox(
                        width: 11,
                      ),
                      Text(
                        _getInfo(),
                        style: TextStyle(fontSize: 12, color: Color(0xFF979CA9)),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Icon(_getIcon(), size: 26, color: Color(0xFF1C1F27),),
          ],
        ),
      ),
    );
  }

  String _getType() {
    switch (type) {
      case OperationType.ADD:
        return 'Начисление';
      case OperationType.OFF:
        return 'Списание';
      case OperationType.REJECT:
      case OperationType.ACCEPT:
        return 'Проверка';
    }
  }

  Color _getColor() {
    switch (type) {
      case OperationType.OFF:
      case OperationType.ADD:
        return Color(0xFF1C1F27);
      case OperationType.REJECT:
        return Color(0xFFED1649);
      case OperationType.ACCEPT:
        return Color(0xFF5CBE5A);
    }
  }

  String _getMessage() {
    switch (type) {
      case OperationType.OFF:
        return '- 1 000 ₽';
      case OperationType.ADD:
        return '+ 1 000 ₽';
      case OperationType.REJECT:
        return 'Отклонил';
      case OperationType.ACCEPT:
        return 'Принято';
    }
  }

  IconData _getIcon() {
    switch (type) {
      case OperationType.ADD:
        return CustomIcons.plus_7_1;
      case OperationType.OFF:
        return CustomIcons.minus_frame;
      case OperationType.REJECT:
      case OperationType.ACCEPT:
        return CustomIcons.profile_7_1;
    }
  }

  String _getInfo() {
    switch (type) {
      case OperationType.ADD:
        return 'Сумма заказа 12 590 ₽';
      case OperationType.OFF:
        return 'Всего бонусов 1 547 ₽';
      case OperationType.REJECT:
      case OperationType.ACCEPT:
        return 'Казань / +7 999 156 1992';
    }
  }
}
