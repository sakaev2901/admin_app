import 'package:admin_app/page/history/filter_button.dart';
import 'package:flutter/material.dart';

class FilterCarousel extends StatelessWidget {
  const FilterCarousel({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 24,
      child: ListView(
        children: [
          SizedBox(width: 17,),
          FilterButton(text: 'Все', isSelected: true,),
          SizedBox(width: 10,),
          FilterButton(text: 'Начисления'),
          SizedBox(width: 10,),
          FilterButton(text: 'Списания'),
          SizedBox(width: 10,),
          FilterButton(text: 'Проверка'),
          SizedBox(width: 17,),
        ],
        scrollDirection: Axis.horizontal,
      ),
    );
  }
}
