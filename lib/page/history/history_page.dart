import 'package:admin_app/page/history/filter_carousel.dart';
import 'package:admin_app/page/history/operation_card.dart';
import 'package:admin_app/page/history/operation_type.dart';
import 'package:admin_app/widgets/app_title.dart';
import 'package:admin_app/widgets/base_page.dart';
import 'package:flutter/material.dart';

class HistoryPage extends StatelessWidget {
  const HistoryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BasePage(
      padding: 0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 17),
              child: AppTitle(title: 'История операций'),
            ),
            SizedBox(height: 33,),
            FilterCarousel(),
            SizedBox(height: 33,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 17),
              child: Column(
                children: [
                  OperationCard(type: OperationType.ADD,),
                  OperationCard(type: OperationType.OFF,),
                  OperationCard(type: OperationType.ACCEPT,),
                  OperationCard(type: OperationType.REJECT,),
                ],
              ),
            ),
          ],
        ),
        selectedPage: 3);
  }
}
