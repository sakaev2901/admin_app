import 'package:flutter/material.dart';

class FilterButton extends StatefulWidget {
  final String text;
  bool isSelected;

  FilterButton({Key? key, required this.text, this.isSelected = false})
      : super(key: key);

  @override
  State<FilterButton> createState() => _FilterButtonState();
}

class _FilterButtonState extends State<FilterButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 5,
        horizontal: 10,
      ),
      decoration: BoxDecoration(
        color: this.widget.isSelected ? Color(0xFF1C1F27) : Colors.white,
        borderRadius: BorderRadius.circular(12),
        border: Border.all(
          color:
              this.widget.isSelected ? Colors.transparent : Color(0xFFFFDD55),
        ),
      ),
      child: Text(
        this.widget.text,
        style: TextStyle(
          color: this.widget.isSelected ? Colors.white : Color(0xFF1C1F27),
        ),
      ),
    );
  }
}
