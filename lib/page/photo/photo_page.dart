import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

import '../../icon/custom_icons_icons.dart';

class PhotoPage extends StatelessWidget {
  final String url;

  const PhotoPage({Key? key, required this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: 9, horizontal: 17),
                child: Row(
                  children: [
                    GestureDetector(
                      onTap: () => Navigator.pop(context),
                      child: Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: Color(0xFFF1F1F1),
                        ),
                        child: Icon(
                          CustomIcons.arrow_left,
                          color: Color(0xFF1C1F27),
                          size: 16,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: ClipRRect(
                  child: PhotoView(
                    imageProvider: NetworkImage(url),
                  ),
                ),
              ),
            ],
          ),
        )
    );
  }
}
