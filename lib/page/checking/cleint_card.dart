import 'package:admin_app/model/simple_client.dart';
import 'package:admin_app/page/client/client_page.dart';
import 'package:admin_app/widgets/custom_divider.dart';
import 'package:admin_app/widgets/custom_ink_well.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ClientCard extends StatelessWidget {
  final SimpleClient client;

  const ClientCard({Key? key, required this.client}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomInkWell(
      onTap: () => Navigator.push(context, CupertinoPageRoute(builder: (_) => ClientPage(id: client.id))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 119,
            padding: const EdgeInsets.symmetric(
              vertical: 16,
            ),
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(12),
                  child: SizedBox(
                    height: 90,
                    width: 90,
                    child: Image.network(
                      client.image,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 19,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        'Город ${client.branch}',
                        style: TextStyle(
                          fontSize: 12,
                        ),
                      ),
                      Text(
                        client.name,
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        padding: const EdgeInsets.symmetric(
                          horizontal: 7,
                          vertical: 3,
                        ),
                        decoration: BoxDecoration(
                          color: const Color(0xFFF1F1F1),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Text(
                          client.phone,
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        ),
                      ),
                      const Text(
                        'Сегодня - 18:32',
                        style: TextStyle(
                            fontSize: 12,
                            color: Color(
                              0xFF979CA9,
                            )),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          CustomDivider.zero(),
        ],
      ),
    );
  }
}
