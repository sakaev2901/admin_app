import 'package:admin_app/model/simple_client.dart';
import 'package:admin_app/page/checking/cleint_card.dart';
import 'package:admin_app/provider/client_provider.dart';
import 'package:admin_app/service/client_service.dart';
import 'package:admin_app/widgets/app_title.dart';
import 'package:admin_app/widgets/base_page.dart';
import 'package:admin_app/widgets/custom_divider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/client_list/client_list_bloc.dart';

class CheckingPage extends StatefulWidget {
  const CheckingPage({Key? key}) : super(key: key);

  @override
  State<CheckingPage> createState() => _CheckingPageState();
}

class _CheckingPageState extends State<CheckingPage> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    BlocProvider.of<ClientListBloc>(context).add(ClientListLoaded());
  }


  @override
  Widget build(BuildContext context) {
    return BasePage(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const AppTitle(title: 'Проверка клиентов'),
          const SizedBox(height: 24,),
          CustomDivider.zero(),
          BlocBuilder<ClientListBloc, ClientListState>(
            builder: (context, state) {
              if (state is ClientListSuccessState) {
                return ClientList(clients: state.clients);
              } else {
                return SizedBox.shrink();
              }
            },
          ),
        ],
      ),
      selectedPage: 2,
    );
  }
}

class ClientList extends StatelessWidget {
  final List<SimpleClient> clients;

  const ClientList({Key? key, required this.clients}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: clients.map((e) => ClientCard(client: e,)).toList(),
    );
  }
}

