import 'package:admin_app/extensions/color_scheme_extension.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../bloc/account_recovery/account_recovery_bloc.dart';
import '../../provider/profile_provider.dart';
import '../../widgets/input/custom_text_input.dart';
import '../../widgets/input/input_type.dart';
import '../../widgets/item_app_bar.dart';
import '../../widgets/main_button.dart';
import 'check_registration_request_screen.dart';

class AccessRecoveryScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AccountRecoveryBloc(ProfileProvider()),
      child: AccessRecoveryView(),
    );
  }
}

class AccessRecoveryView extends StatefulWidget {
  const AccessRecoveryView({Key? key}) : super(key: key);

  @override
  _AccessRecoveryViewState createState() => _AccessRecoveryViewState();
}

class _AccessRecoveryViewState extends State<AccessRecoveryView> {
  final TextEditingController _phoneController = TextEditingController();

  @override
  void initState() {
    super.initState();
    BlocProvider.of<AccountRecoveryBloc>(context).stream.listen((state) {
      if (state is AccountRecoveryLoadedSuccess) {
        Navigator.of(context).push(CupertinoPageRoute(
            builder: (_) => SuccessRequestScreen(
                  barTitle: 'Восстановить пароль',
                  mainTitle: 'Отлично!\nМы отправили ссылку',
                  text:
                      'Проверьте вашу электронную почту\nи выполните восстановление\nпо ссылке в письме.',
                )));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          bottom: false,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal:  17),
            child: Stack(
              children: [
                Column(
                  children: [
                    SizedBox(
                      height: 17,
                    ),
                    // ItemAppBar(title: 'Восстановить доступ',),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Container(
                          constraints: BoxConstraints(
                              minHeight:
                                  MediaQuery.of(context).size.height - 50),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset('assets/access.svg'),
                              SizedBox(
                                height: 48,
                              ),
                              Text(
                                'Восстановление доступа',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 30, fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: 24,
                              ),
                              Text(
                                'Для восстановления доступа\nвведите номер телефона\nна привязанную почту мы отправим ссылку\nдля восстановления пароля.',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 16,
                                  height: 1.34,
                                  color: Theme.of(context)
                                      .colorScheme
                                      .chipTextColor,
                                ),
                              ),
                              SizedBox(
                                height: 26,
                              ),
                              BlocBuilder<AccountRecoveryBloc,
                                  AccountRecoveryState>(
                                builder: (context, state) {
                                  if (state is AccountRecoveryLoadedFail) {
                                    return Row(
                                      children: [
                                        Text(
                                          state.error ?? '',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontSize: 16,
                                            height: 1.34,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.red,
                                          ),
                                        ),
                                      ],
                                    );
                                  } else {
                                    return Container();
                                  }
                                },
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              BlocBuilder<AccountRecoveryBloc,
                                  AccountRecoveryState>(
                                builder: (context, state) {
                                  bool withError = false;
                                  if (state is AccountRecoveryLoadedFail) {
                                    withError = true;
                                  }
                                  return CustomTextInput(
                                    label: 'Мобильный телефон',
                                    hint: '+7 XXX XXX XXXX',
                                    inputType: InputType.PHONE,
                                    withError: withError,
                                    controller: _phoneController,
                                  );
                                },
                              ),
                              SizedBox(
                                height: 23,
                              ),
                              MainButton(
                                text: 'Отправить',
                                onPressed: () {
                                  BlocProvider.of<AccountRecoveryBloc>(context)
                                      .add(AccountRecoveryButtonPressed(
                                          _phoneController.text));
                                },
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                // Positioned(left: 0, right: 0, top: 17,child: Container(color:Colors.white,child: Text('Восстановить пароль', textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.w700, color: Theme.of(context).colorScheme.secondaryTextColor,),)))
                Positioned(
                    left: 0,
                    right: 0,
                    top: 17,
                    child: ItemAppBar(
                      title: 'Восстановить доступ',
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
