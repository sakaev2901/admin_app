import 'package:admin_app/extensions/color_scheme_extension.dart';
import 'package:admin_app/page/login/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../widgets/main_button.dart';

class SuccessRequestScreen extends StatelessWidget {

  final String barTitle;
  final String mainTitle;
  final String text;


  SuccessRequestScreen({Key? key,required this.barTitle,required this.mainTitle,required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 17),
          child: Stack(
            children: [
              Column(
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      child: Container(
                        constraints: BoxConstraints(minHeight: MediaQuery.of(context).size.height - 50),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset('assets/svg/comp.svg'),
                            SizedBox(height: 48,),
                            Text(this.mainTitle, textAlign: TextAlign.center, style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),),
                            SizedBox(height: 24,),
                            Text(this.text,
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 16, height: 1.34, color: Theme.of(context).colorScheme.chipTextColor,),
                            ),
                            SizedBox(height: 34,),
                            MainButton(text: 'Прекрасно', onPressed: () => Navigator.of(context).push(
                                CupertinoPageRoute(builder: (_) => LoginPage())),)
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Positioned(left: 0, right: 0, top: 17,child: Text(this.barTitle, textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.w700, color: Theme.of(context).colorScheme.secondaryTextColor,),))
            ],
          ),
        ),
      ),
    );
  }
}
