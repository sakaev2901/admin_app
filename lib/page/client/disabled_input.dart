import 'package:flutter/material.dart';

class DisabledInput extends StatelessWidget {
  final String label;
  final String value;
  final double padding;
  IconData? icon;
  DisabledInput({Key? key,required this.label,required this.value,required this.padding, this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: padding),
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 11,),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: Color(0xFFF1F1F1),
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(label, style: TextStyle(fontSize: 12, color: Color(0xFF979CA9)),),
                  SizedBox(height: 6,),
                  Text(value, style: TextStyle(fontSize: 16,),),
                ],
              ),
            ),
            if (icon != null)
            Container(
              height: 36,
              width: 36,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
              ),
              child: Align(
                alignment: Alignment.center,
                child: Icon(
                  icon,
                  size: 24,
                  color: const Color(0xFF1C1F27),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
