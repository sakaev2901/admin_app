import 'package:admin_app/bloc/client_list/client_list_bloc.dart';
import 'package:admin_app/icon/custom_icons_icons.dart';
import 'package:admin_app/model/client.dart';
import 'package:admin_app/page/client/data_title.dart';
import 'package:admin_app/page/client/disabled_input.dart';
import 'package:admin_app/page/client/photo_card.dart';
import 'package:admin_app/provider/client_provider.dart';
import 'package:admin_app/widgets/main_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/client/client_bloc.dart';
import '../../bloc/validation/validation_bloc.dart';

class ClientPage extends StatelessWidget {
  final int id;

  const ClientPage({Key? key, required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(id);
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
          ClientBloc(ClientProvider())
            ..add(ClientLoaded(id)),
        ),
        BlocProvider(
          create: (context) => ValidationBloc(ClientProvider(), BlocProvider.of<ClientListBloc>(context)),
        ),
      ],
      child: ClientView(),
    );
  }
}

class ClientView extends StatelessWidget {
  const ClientView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 17, vertical: 9),
          child: BlocBuilder<ClientBloc, ClientState>(
            builder: (context, state) {
              if (state is ClientLoadedSuccess) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        GestureDetector(
                          onTap: () => Navigator.pop(context),
                          child: Container(
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: Color(0xFFF1F1F1),
                            ),
                            child: Icon(
                              CustomIcons.arrow_left,
                              color: Color(0xFF1C1F27),
                              size: 16,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Expanded(
                            child: Text(
                              state.client.name ?? "-",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            )),
                      ],
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 41,
                            ),
                            DataTitle(title: 'Контакты'),
                            DisabledInput(
                                label: 'Ваше имя',
                                value: state.client.name ?? "-",
                                padding: 20),
                            DisabledInput(
                                label: 'Номер телефона',
                                value: state.client.phone ?? '-',
                                padding: 20),
                            DisabledInput(
                                label: 'Электронная почта',
                                value: state.client.email ?? '-',
                                padding: 20),
                            DisabledInput(
                                label: 'Дата рождения',
                                value: state.client.dateBirth ?? '-',
                                padding: 0),
                            SizedBox(
                              height: 41,
                            ),
                            DataTitle(title: 'Паспортные данные'),
                            DisabledInput(
                                label: 'Серия документа',
                                value: state.client.docNumber ?? '-',
                                padding: 20),
                            DisabledInput(
                                label: 'Серия документа',
                                value: state.client.docSeries ?? '-',
                                padding: 20),
                            SizedBox(
                              height: 41,
                            ),
                            DataTitle(title: 'Сканы документа'),
                            GridView(
                              shrinkWrap: true,
                              primary: false,
                              gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                crossAxisSpacing: 10,
                              ),
                              children: _buildPhotoCards(
                                  state.client.passportPhotos),
                            ),
                            SizedBox(
                              height: 42,
                            ),
                            MainButton(
                              text: 'Одобрить заявку клиента',
                              onPressed: () => BlocProvider.of<ValidationBloc>(context).add(ValidationSubmitted(state.client.id)),
                            ),
                            SizedBox(
                              height: 18,
                            ),
                            Container(
                              width: double.infinity,
                              padding: EdgeInsets.symmetric(
                                vertical: 14,
                              ),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                border: Border.all(
                                  color: Color(0xFFF1F1F1),
                                ),
                              ),
                              child: Align(
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Отклонить заявку клиента',
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700,
                                        color: Color(0xFF979CA9)),
                                  )),
                            ),
                            SizedBox(
                              height: 11,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                );
              }
              return Column();
            },
          ),
        ),
      ),
    );
  }

  List<PhotoCard> _buildPhotoCards(List<String> passportPhotos) {
    return passportPhotos.map((e) => PhotoCard(url: e)).toList();
  }
}
