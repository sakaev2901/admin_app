import 'package:flutter/material.dart';

class DataTitle extends StatelessWidget {
  final String title;
  const DataTitle({Key? key,required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 17),
      child: Text(title, style: TextStyle(fontSize: 20, color: Color(0xFF979CA9)),),
    );
  }
}
