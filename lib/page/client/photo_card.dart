import 'package:admin_app/page/photo/photo_page.dart';
import 'package:flutter/material.dart';

class PhotoCard extends StatelessWidget {
  final String url;

  const PhotoCard({Key? key, required this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => PhotoPage(url: url))),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(12),
        child: SizedBox(
          child: Image.network(
            url,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
