import 'package:admin_app/bloc/client_bonus_info/client_bonus_info_bloc.dart';
import 'package:admin_app/icon/custom_icons_icons.dart';
import 'package:admin_app/page/add_bonuses/confirming_modal.dart';
import 'package:admin_app/page/add_bonuses/info_card.dart';
import 'package:admin_app/page/client/disabled_input.dart';
import 'package:admin_app/widgets/app_title.dart';
import 'package:admin_app/widgets/base_page.dart';
import 'package:admin_app/widgets/bonus_title.dart';
import 'package:admin_app/widgets/hint/add_bonuses_skeleton.dart';
import 'package:admin_app/widgets/hint/big_hint.dart';
import 'package:admin_app/widgets/hint/input_hint.dart';
import 'package:admin_app/widgets/hint/small_hint.dart';
import 'package:admin_app/widgets/input/account_input.dart';
import 'package:admin_app/widgets/input/custom_input.dart';
import 'package:admin_app/widgets/main_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AddBonusesPage extends StatelessWidget {
  const AddBonusesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AddBonusesView();
  }
}

class AddBonusesView extends StatefulWidget {
  const AddBonusesView({Key? key}) : super(key: key);

  @override
  State<AddBonusesView> createState() => _AddBonusesViewState();
}

class _AddBonusesViewState extends State<AddBonusesView> {
  void _showModal() {
    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
            backgroundColor: Colors.transparent,
            insetPadding: EdgeInsets.all(10),
            child: Stack(
              children: [
                ConfirmingModal(),
              ],
            ));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return BasePage(
      selectedPage: 0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const AppTitle(title: 'Начислить бонусы'),
          const SizedBox(
            height: 32,
          ),
          const BonusTitle(
            icon: CustomIcons.plus_7_1,
          ),
          const SizedBox(
            height: 36,
          ),
          CustomInput(
            icon: CustomIcons.wallet,
            label: 'Сумма заказа клиента',
          ),
          const SizedBox(
            height: 16,
          ),
          AccountInput(
            onConfirm: (phone) => BlocProvider.of<ClientBonusInfoBloc>(context)
                .add(ClientBonusInfoLoaded(phone)),
            icon: CustomIcons.profile_7_1,
            label: 'Номер лицевого счета',
          ),
          const SizedBox(
            height: 16,
          ),
          BlocBuilder<ClientBonusInfoBloc, ClientBonusInfoState>(
            builder: (context, state) {
              if (state is ClientBonusInfoLoadedSuccess) {
                return Column(
                  children: [
                    InfoCard(),
                    const SizedBox(
                      height: 16,
                    ),
                    DisabledInput(
                      label: 'Будет начислено бонусов',
                      value: '500 ₽',
                      padding: 0,
                      icon: CustomIcons.plus_7_1,
                    ),
                    const SizedBox(
                      height: 36,
                    ),
                    MainButton(
                      text: 'Начислить бонусы',
                      onPressed: () => _showModal(),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                  ],
                );
              } else if (state is ClientBonusInfoLoadingInProgress) {
                return const AddBonusesSkeleton();
              }
              return Container();
            },
          ),
        ],
      ),
    );
  }
}
