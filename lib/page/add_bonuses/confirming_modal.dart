import 'package:admin_app/page/history/operation_card.dart';
import 'package:admin_app/page/history/operation_type.dart';
import 'package:admin_app/widgets/main_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ConfirmingModal extends StatelessWidget {
  const ConfirmingModal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Container(
      padding: const EdgeInsets.all(20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(24),
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 18,),
              SvgPicture.asset('assets/comp.svg'),
              SizedBox(height: 28,),
              Text(
                'Операция выполнена! Бонусы начислены.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 16,),
              Text(
                'Бонусы поступят в течении 12 часов после вашей операции',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16, color: Color(0xFF979CA9), height: 1.2),
              ),
              SizedBox(height: 21,),
              OperationCard(type: OperationType.ADD),
              SizedBox(height: 16,),
              MainButton(text: 'Сохранить операцию', onPressed: () => Navigator.pop(context),),
              SizedBox(height: 16,),
              MainButton(text: 'Отменить операцию', color: Color(0xFFF1F1F1), textColor: Color(0xFF86888F), onPressed: () => Navigator.pop(context),),
            ],
          ),
        ),
      );
  }
}
