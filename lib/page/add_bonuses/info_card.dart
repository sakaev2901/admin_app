import 'package:flutter/material.dart';

class InfoCard extends StatelessWidget {
  const InfoCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Color(0xFFE9FFB9),
      ),
      padding: EdgeInsets.symmetric(vertical: 5),
      width: double.infinity,
      child: Align(alignment: Alignment.center, child: Text('Хакимов Айрат • Silver > 10%', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),)),
    );
  }
}
