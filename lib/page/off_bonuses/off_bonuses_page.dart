import 'package:admin_app/icon/custom_icons_icons.dart';
import 'package:admin_app/page/add_bonuses/info_card.dart';
import 'package:admin_app/page/client/disabled_input.dart';
import 'package:admin_app/widgets/app_title.dart';
import 'package:admin_app/widgets/base_page.dart';
import 'package:admin_app/widgets/bonus_title.dart';
import 'package:admin_app/widgets/hint/big_hint.dart';
import 'package:admin_app/widgets/hint/input_hint.dart';
import 'package:admin_app/widgets/hint/small_hint.dart';
import 'package:admin_app/widgets/input/account_input.dart';
import 'package:admin_app/widgets/input/custom_input.dart';
import 'package:admin_app/widgets/main_button.dart';
import 'package:flutter/material.dart';

class OffBonusesPage extends StatelessWidget {
  const OffBonusesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BasePage(
      selectedPage: 1,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AppTitle(title: 'Списать бонусы'),
          SizedBox(height: 32,),
          BonusTitle(icon: CustomIcons.minus_frame,),
          SizedBox(height: 36,),
          AccountInput(icon: CustomIcons.profile_7_1, label: 'Номер лицевого счета',),
          SizedBox(height: 16,),
          InfoCard(),
          SizedBox(height: 16,),
          DisabledInput(label: 'Доступно к списанию бонусов', value: '1 420 ₽', padding: 16, icon: CustomIcons.discount,),
          CustomInput(icon: CustomIcons.minus_frame, label: 'Количество списываемых бонусов'),
          SizedBox(height: 36,),
          MainButton(text: 'Списать бонусы',),
          SizedBox(height: 16,),
          SmallHint(),
          SizedBox(height: 16,),
          InputHint(icon: CustomIcons.discount,),
          SizedBox(height: 16,),
          InputHint(icon: CustomIcons.minus_frame,),
          SizedBox(height: 36,),
          BigHint(),
        ],
      ),
    );
  }
}
