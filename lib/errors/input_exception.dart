class InputException implements Exception {
  final String text;
  final int index;

  InputException(this.text, this.index);
}