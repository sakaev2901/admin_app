import 'dart:ui';

class BaseException implements Exception {
  final String cause;

  BaseException(this.cause);

  @override
  String toString() {
    return cause;
  }
}