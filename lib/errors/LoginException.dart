class LoginException implements Exception {
  final String cause;

  LoginException(this.cause);
}