import 'package:flutter/material.dart';

class CustomInkWell extends StatelessWidget {
  GestureTapCallback? onTap;
  Widget? child;
  BorderRadius? borderRadius;

  CustomInkWell({this.onTap, this.child, this.borderRadius});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        borderRadius: this.borderRadius,
        onTap: this.onTap,
        child: this.child,
      ),
    );
  }
}
