import 'package:admin_app/extensions/color_scheme_extension.dart';
import 'package:flutter/material.dart';
import 'custom_ink_well.dart';

class BaseIconButton extends StatelessWidget {
  GestureTapCallback? onTap;
  final IconData icon;
  final int iconSize;
  final Color? color;

  BaseIconButton({Key? key,required this.icon, this.iconSize = 20, this.onTap, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: 40,
      decoration: BoxDecoration(
        borderRadius:
        BorderRadius.circular(12),
        color: Theme.of(context).accentColor,
      ),
      child: CustomInkWell(
        onTap: this.onTap,
        borderRadius:
        BorderRadius.circular(12),
        child: Icon(
          this.icon,
          size: this.iconSize.toDouble(),
          color: this.color ??
          Theme.of(context).colorScheme.selectedIconColor,
        ),
      ),
    );
  }
}
