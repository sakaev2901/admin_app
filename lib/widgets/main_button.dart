import 'package:admin_app/extensions/color_scheme_extension.dart';
import 'package:flutter/material.dart';

class MainButton extends StatelessWidget {
  final String text;
  final VoidCallback? onPressed;
  bool enabled;
  Color? color;
  Color? textColor;

  MainButton({Key? key, this.text = '', this.onPressed, this.enabled = true, this.color, this.textColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (color == null) {
      this.color = Theme.of(context).primaryColor;
    }
    if (textColor == null) {
      this.textColor = Theme.of(context).colorScheme.primaryTextColor;
    }
    return TextButton(
      onPressed: this.onPressed,
      style: TextButton.styleFrom(
        primary: Theme.of(context).colorScheme.selectedIconColor,
        minimumSize: Size(double.infinity, 54),
        backgroundColor: this.enabled ? this.color : Theme.of(context).accentColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0),
        ),
      ),
      child: Text(text, style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: this.enabled ? this.textColor : Color(0xFF86888F)),),
    );
  }
}
