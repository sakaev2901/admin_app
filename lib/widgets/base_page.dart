import 'package:admin_app/widgets/bottom_bar/bottom_nav_bar.dart';
import 'package:flutter/material.dart';

class BasePage extends StatelessWidget {
  final Widget child;
  final int selectedPage;
  bool showBottomBar;
  double padding;
  BasePage({Key? key,required this.child,required this.selectedPage, this.padding = 17, this.showBottomBar = true}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: SingleChildScrollView(
                padding: EdgeInsets.symmetric(horizontal: this.padding),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 17,),
                    this.child,
                  ],
                )
              ),
            ),
            if (showBottomBar)
            BottomNavBar(selectedPage: selectedPage,),
          ],
        ),
      ),
    );
  }
}
