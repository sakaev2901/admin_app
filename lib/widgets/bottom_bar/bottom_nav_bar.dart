import 'package:admin_app/icon/custom_icons_icons.dart';
import 'package:admin_app/page/add_bonuses/add_bonuses_page.dart';
import 'package:admin_app/page/checking/checking_page.dart';
import 'package:admin_app/page/history/history_page.dart';
import 'package:admin_app/page/off_bonuses/off_bonuses_page.dart';
import 'package:admin_app/page/profile/profile_page.dart';
import 'package:flutter/material.dart';

import 'bottom_nav_bar_button.dart';

class BottomNavBar extends StatelessWidget {
  int? selectedPage;

  BottomNavBar({Key? key, this.selectedPage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 86,
      padding: EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
        color: Color(0xFFF1F1F1),
      ),
      child: Row(
        children: [
          BottomNavBarButton(
            icon: CustomIcons.plus_7_1,
            label: 'Начислить',
            screen: AddBonusesPage(),
            isSelected: this.selectedPage == 0,
          ),
          BottomNavBarButton(
            icon: CustomIcons.minus_frame,
            label: 'Списание',
            screen: OffBonusesPage(),
            isSelected: this.selectedPage == 1,
          ),
          BottomNavBarButton(
            icon: CustomIcons.category_7_1,
            label: 'Проверки',
            screen: CheckingPage(),
            isSelected: this.selectedPage == 2,
          ),
          BottomNavBarButton(
            icon: CustomIcons.document,
            label: 'История',
            screen: HistoryPage(),
            isSelected: this.selectedPage == 3,
          ),
          BottomNavBarButton(
            icon: CustomIcons.profile_7_1,
            label: 'Профиль',
            screen: ProfilePage(),
            isSelected: this.selectedPage == 4,
          ),
        ],
      ),
    );
  }
}
