import 'package:admin_app/widgets/custom_ink_well.dart';
import 'package:flutter/material.dart';

class BottomNavBarButton extends StatelessWidget {
  final IconData icon;
  final String label;
  bool isSelected;
  final Widget screen;
  final bool isBadged;
  final int count;

  BottomNavBarButton({
    Key? key,
    required this.icon,
    required this.label,
    required this.screen,
    this.isSelected = false,
    this.isBadged = false,
    this.count = 1,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: CustomInkWell(
        onTap: () => Navigator.pushReplacement(
          context,
          PageRouteBuilder(
            pageBuilder: (context, animation1, animation2) => this.screen,
            transitionDuration: Duration(seconds: 0),
          ),
        ),
        child: Stack(
          children: [
            Container(
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    this.icon,
                    size: 24,
                    color: this.isSelected
                        ? Color(0xFF1C1F27)
                        : Color(0xFF6F7481),
                  ),
                  SizedBox(height: 10),
                  Text(
                    this.label,
                    style: TextStyle(
                      fontSize: 10,
                      color: this.isSelected
                          ? Color(0xFF1C1F27)
                          : Color(0xFF6F7481),
                    ),
                  )
                ],
              ),
            ),
            Visibility(
              visible: this.isBadged,
              child: Positioned(
                  right: 3,
                  top: 3,
                  child: Container(
                    height: 20,
                    width: 20,
                    child: Align(
                      alignment: Alignment.center,
                      child: Text('${this.count}', style: TextStyle(fontWeight: FontWeight.w700),),
                    ),
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(10)),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
