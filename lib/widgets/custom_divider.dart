import 'package:flutter/material.dart';

class CustomDivider extends StatelessWidget {

  final double height;
  Color? color = Colors.red;
  double? thickness = 1;

  CustomDivider({Key? key, this.height = 16, this.color, this.thickness = 1}) : super(key: key);

  CustomDivider.zero({this.color, this.thickness = 1}) : this.height = 0;

  @override
  Widget build(BuildContext context) {
    return Divider(
      thickness: thickness,
      height: height,
      color: Color(0xFFF2F2F2),
    );
  }
}
