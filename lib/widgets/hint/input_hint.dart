import 'package:flutter/material.dart';

class InputHint extends StatelessWidget {
  final IconData icon;
  const InputHint({Key? key,required this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 12,),
      height: 60,
      width: double.infinity,
      decoration: BoxDecoration(
        color: const Color(0xFFF1F1F1),
        borderRadius: BorderRadius.circular(12),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 9,
                width: 149,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: const Color(0xFFCFD5D9),
                ),
              ),
              const Spacer(),
              Container(
                height: 14,
                width: 60,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(13),
                  color: const Color(0xFFCFD5D9),
                ),
              ),
            ],
          ),
          Spacer(),
          Container(
            height: 36,
            width: 36,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
            ),
            child: Align(
              alignment: Alignment.center,
              child: Icon(
                icon,
                size: 24,
                color: const Color(0xFFCFD5D9),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
