import 'package:admin_app/widgets/hint/small_hint.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import '../../icon/custom_icons_icons.dart';
import 'big_hint.dart';
import 'input_hint.dart';

class OffBonusesSkeleton extends StatelessWidget {
  const OffBonusesSkeleton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Color baseColor = Theme.of(context).accentColor;
    final Color highlightColor = Colors.white;
    return Shimmer.fromColors(
      baseColor: baseColor,
      highlightColor: highlightColor,
      child: Column(
        children: const [
          SmallHint(),
          SizedBox(
            height: 16,
          ),
          InputHint(
            icon: CustomIcons.plus_7_1,
          ),
          SizedBox(
            height: 16,
          ),
          InputHint(icon: CustomIcons.minus_frame,),
          SizedBox(
            height: 36,
          ),
          BigHint(),
        ],
      ),
    );
  }
}
