import 'package:flutter/material.dart';

class SmallHint extends StatelessWidget {
  const SmallHint({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 26,
      width: double.infinity,
      decoration: BoxDecoration(
        color: Color(0xFFF1F1F1),
        borderRadius: BorderRadius.circular(12),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 9,
            width: 118,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(9),
              color: Color(0xFFCFD5D9),
            ),
          ),
          const SizedBox(width: 5,),
          Container(
            height: 3,
            width: 3,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(1.5),
              color: Color(0xFFCFD5D9),
            ),
          ),
          const SizedBox(width: 5,),
          Container(
            height: 9,
            width: 58,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(9),
              color: Color(0xFFCFD5D9),
            ),
          ),
        ],
      ),
    );
  }
}
