import 'package:admin_app/extensions/color_scheme_extension.dart';
import 'package:admin_app/widgets/input/phone_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'input_type.dart';
import 'masked_text_input_formatter.dart';

class CustomTextInput extends StatefulWidget {
  final InputType inputType;
  final String label;
  final String hint;
  bool disabled;
  double bottomMargin;
  String value = '';
  TextEditingController? controller;
  ValueChanged<int?>? onChanged;
  int listValue;
  List<Widget> items;
  bool withError;
  Function(String)? onChangeText;

  CustomTextInput({
    Key? key,
    required this.label,
    required this.hint,
    this.bottomMargin = 0,
    this.inputType = InputType.STRING,
    this.controller,
    this.onChanged,
    this.disabled = false,
    this.listValue = 1,
    this.items = const [],
    this.withError = false,
    this.onChangeText,
  }) : super(key: key);

  @override
  _CustomTextInputState createState() => _CustomTextInputState();
}

class _CustomTextInputState extends State<CustomTextInput> {
  String value = '';
  String obscureText = '';
  late FocusNode focusNode;

  @override
  void initState() {
    super.initState();

    focusNode = FocusNode();
  }

  List<TextInputFormatter> _buildFormatters() {
    switch (this.widget.inputType) {
      case InputType.PHONE:
        {
          return [
            MaskedTextInputFormatter(mask: '+7 XXX XXX XXXX', separator: ' '),
            PhoneFormatter(),
          ];
        }
      case InputType.CARD_NUMBER:
        {
          return [
            MaskedTextInputFormatter(
                mask: 'xxxx xxxx xxxx xxxx', separator: ' ')
          ];
        }
      case InputType.CVV:
        {
          return [MaskedTextInputFormatter(mask: 'xxx', separator: '')];
        }
      case InputType.CARD_DATE:
        {
          return [MaskedTextInputFormatter(mask: 'XX/XX', separator: '/')];
        }
      case InputType.DATE:
        {
          return [MaskedTextInputFormatter(mask: 'XX.XX.XXXX', separator: '.')];
        }
      case InputType.PASSPORT:
        {
          return [
            MaskedTextInputFormatter(mask: 'xxxx xxxxxx', separator: ' ')
          ];
        }
      case InputType.TIME:
        {
          return [
            MaskedTextInputFormatter(mask: 'XX:XX', separator: ':')
          ];
        }
      default:
        return [];
    }
  }

  TextInputType _getKeyboardType() {
    switch (this.widget.inputType) {
      case InputType.CARD_NUMBER:
      case InputType.CVV:
      case InputType.CARD_DATE:
      case InputType.PASSPORT:
      case InputType.NUMBER:
      case InputType.TIME:
        return TextInputType.number;
      case InputType.PHONE:
        return TextInputType.phone;
      case InputType.MAIL:
        return TextInputType.emailAddress;
      default:
        return TextInputType.text;
    }
  }

  List<DropdownMenuItem<int>> _buildDropList() {
    List<DropdownMenuItem<int>> items = [];
    for (int i = 0; i < this.widget.items.length; i++) {
      items.add(DropdownMenuItem(
        child: this.widget.items[i],
        value: i,
      ));
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () => focusNode.requestFocus(),
          child: Container(
            padding: EdgeInsets.only(
              left: this.widget.inputType == InputType.PASSWORD ? 8 : 14,
              top: 10,
              bottom: 10,
              right: 14,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(9),
              border:
                  Border.all(color: this.widget.withError ? Colors.red : Theme.of(context).accentColor, width: 1),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(
                      left:
                          this.widget.inputType == InputType.PASSWORD ? 6 : 0),
                  child: Text(
                    this.widget.label,
                    style: TextStyle(
                      fontSize: 12,
                      color: this.value != ''
                          ? Theme.of(context).colorScheme.chipTextColor
                          : Theme.of(context).colorScheme.primaryTextColor,
                    ),
                  ),
                ),
                SizedBox(
                  height: 9,
                ),
                Container(
                  child: this.widget.inputType == InputType.LIST
                      ? Container(
                          width: double.infinity,
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                              dropdownColor: Colors.white,
                              isDense: true,
                              value: this.widget.listValue,
                              items: _buildDropList(),
                              onChanged: this.widget.onChanged,
                            ),
                          ),
                        )
                      : TextField(
                          focusNode: focusNode,
                          controller: this.widget.controller,
                          readOnly: this.widget.disabled,
                          onChanged: (val) {
                            setState(() {
                              // this.obscureText += '•';
                              // _textController.text = this.obscureText;
                              if (this.widget.onChangeText != null) {
                                this.widget.onChangeText!(val);
                              }
                              this.value = val;
                            });
                          },
                          obscureText:
                              this.widget.inputType == InputType.PASSWORD
                                  ? true
                                  : false,
                          obscuringCharacter: '•',
                          enabled: true,
                          // inputFormatters: [MaskedTextInputFormatter(mask: 'xxxx', separator: '')],
                          inputFormatters: [..._buildFormatters()],
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: this.widget.inputType == InputType.TIME ? FontWeight.bold : FontWeight.normal,
                            letterSpacing:
                                this.widget.inputType == InputType.PASSWORD
                                    ? 12
                                    : 0,
                          ),
                          keyboardType: _getKeyboardType(),
                          decoration: InputDecoration(
                            isDense: true,
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.symmetric(vertical: 0),
                            hintText: widget.hint,
                            hintStyle: TextStyle(
                              color:
                                  Theme.of(context).colorScheme.accentTextColor,
                              fontSize: 16,
                            ),
                          ),
                        ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: this.widget.bottomMargin,
        ),
      ],
    );
  }
}
