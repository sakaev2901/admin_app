import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

class PasswordTextInput extends TextInputFormatter {

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.length > oldValue.text.length) {
      String text = '';
      if (oldValue.text.length == 0) {
        text = '*';
      } else {
        text = oldValue.text + '*';
      }
      return TextEditingValue(
        text: text,
        selection: TextSelection.collapsed(offset: newValue.text.length)
      );
    }
    return newValue;
  }

}