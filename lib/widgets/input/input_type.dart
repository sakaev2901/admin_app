enum InputType {
  NUMBER, PHONE, CARD_DATE, STRING, PASSWORD, CARD_NUMBER, CVV, MAIL, PASSPORT, LIST, DATE, TIME
}