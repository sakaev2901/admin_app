import 'package:admin_app/widgets/custom_ink_well.dart';
import 'package:admin_app/widgets/input/custom_text_field.dart';
import 'package:barcode_scan2/barcode_scan2.dart';
import 'package:flutter/material.dart';

class AccountInput extends StatelessWidget {
  final IconData icon;
  final String label;
  final bool withQr;
  final Function(String)? onConfirm;
  final TextEditingController _controller = TextEditingController();


  AccountInput(
      {Key? key, required this.icon, required this.label, this.withQr = false, this.onConfirm})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 14,
        vertical: 12,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        border: Border.all(color: const Color(0xFFF1F1F1)),
      ),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      label,
                      style: const TextStyle(color: Color(0xFF979CA9), fontSize: 12),
                    ),
                    const SizedBox(
                      height: 6,
                    ),
                    CustomTextField(controller: _controller, onConfirm: onConfirm,),
                  ],
                ),
              ),
              Container(
                height: 36,
                width: 36,
                decoration: BoxDecoration(
                  color: const Color(0xFFF1F1F1),
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Align(
                  alignment: Alignment.center,
                  child: Icon(
                    icon,
                    size: 24,
                    color: const Color(0xFF1C1F27),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 11,),
          CustomInkWell(
            onTap: () async {
              var result = await BarcodeScanner.scan();
              _controller.text = result.rawContent;
            },
            child: Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(vertical: 10,),
              decoration: BoxDecoration(
                color: const Color(0xFFFFDD55),
                borderRadius: BorderRadius.circular(
                  12,
                ),
              ),
              child: const Align(
                alignment: Alignment.center,
                child: Text(
                  'Отсканировать QR код',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
