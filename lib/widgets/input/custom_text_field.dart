import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  TextEditingController? controller;
  final Function(String)? onConfirm;

  CustomTextField({Key? key, this.controller, this.onConfirm}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    controller = controller ?? TextEditingController();
    return TextField(
      controller: controller!,
      onSubmitted: onConfirm,
      decoration: InputDecoration(
        isDense: true,
        border: InputBorder.none,
        contentPadding: EdgeInsets.symmetric(vertical: 0),
        hintText: 'тест',
        hintStyle: TextStyle(
          color: Color(0xFF1C1F27),
          fontSize: 16,
        ),
      ),
    );
  }
}
