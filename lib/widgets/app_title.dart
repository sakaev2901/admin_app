import 'package:flutter/material.dart';

class AppTitle extends StatelessWidget {
  final String title;
  const AppTitle({Key? key,required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(this.title, style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, height: 1.1),);
  }
}
