import 'package:admin_app/icon/custom_icons_icons.dart';
import 'package:flutter/material.dart';

class BonusTitle extends StatelessWidget {
  final IconData icon;

  const BonusTitle({Key? key, required this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Stack(
            children: [
              Container(
                width: 87,
              ),
              Container(
                height: 94,
                width: 58,
                decoration: BoxDecoration(
                  color: Color(0xFFF1F1F1),
                  borderRadius: BorderRadius.circular(14),
                ),
              ),
              Positioned.fill(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Icon(
                    icon,
                    size: 60,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(width: 19,),
          const Expanded(
              child: Text(
            'Введите данные. Проверьте информацию об аккаунте клиента',
            style: TextStyle(
              fontSize: 18,
              color: Color(0xFF979CA9),
            ),
          ))
        ],
      ),
    );
  }
}
