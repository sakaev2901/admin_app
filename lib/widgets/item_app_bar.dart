import 'package:admin_app/extensions/color_scheme_extension.dart';
import 'package:flutter/material.dart';
import '../icon/custom_icons_icons.dart';
import 'base_icon_button.dart';

class ItemAppBar extends StatefulWidget {

  BaseIconButton? secondButton;
  final String title;

  GestureTapCallback? onTapBack;
  bool isFavorite = false;


  ItemAppBar({Key? key,this.secondButton, this.onTapBack, this.isFavorite = false, required this.title}) : super(key: key);

  @override
  _ItemAppBarState createState() => _ItemAppBarState();
}

class _ItemAppBarState extends State<ItemAppBar> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        BaseIconButton(icon: CustomIcons.arrow_left, iconSize: 16, onTap: () {
          print(Navigator.canPop(context) );
          if (Navigator.canPop(context)) {
            Navigator.pop(context);
          }
        },),
        Text(this.widget.title, style: TextStyle(color: Theme.of(context).colorScheme.secondaryTextColor, fontWeight: FontWeight.w700),),
        if (this.widget.secondButton != null)
        this.widget.secondButton!
        else SizedBox(width: 40,),
      ],
    );
  }
}
