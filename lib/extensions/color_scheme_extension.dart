import 'package:flutter/material.dart';

extension CustomColorScheme on ColorScheme {
  Color get defaultIconColor => const Color(0xFF6F7481);
  Color get selectedIconColor => const Color(0xFF1C1F27);
  Color get secondaryIconColor => const Color(0xFF6D6F73);
  Color get accentTextColor => const Color.fromRGBO(28, 31, 39, 0.4);
  Color get checkboxBorderColor => const Color(0xFFD1D1D1);
  Color get secondaryTextColor => const Color(0xFF6D6F73);
  Color get primaryTextColor => const Color(0xFF1C1F27);
  Color get switchButtonColor => const Color(0xFF1C1F27);
  Color get chipTextColor => const Color(0xFF979BA5);
  Color get discountTextColor => const Color(0xFFF02222);
  Color get activeButtonColor => const Color(0xFF1C1F27);

}