import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'client.g.dart';

@JsonSerializable()
class Client extends Equatable {
  final int id;
  final String? phone;
  final String? name;
  final String? email;
  @JsonKey(name: 'date_birth')
  final String? dateBirth;
  @JsonKey(name: 'doc_series')
  final String? docSeries;
  @JsonKey(name: 'doc_number')
  final String? docNumber;
  @JsonKey(name: 'passport_photos')
  final List<String> passportPhotos;
  final int branch;

  Client(
      {required this.phone,
        required this.id,
      required this.name,
      required this.branch,
      required this.email,
      required this.docNumber,
      required this.dateBirth,
      required this.docSeries,
      required this.passportPhotos});

  factory Client.fromJson(Map<String, dynamic> json) =>
      _$ClientFromJson(json);

  @override
  List<Object?> get props => [phone, name, branch];
}
