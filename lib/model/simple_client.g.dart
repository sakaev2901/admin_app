// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'simple_client.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SimpleClient _$SimpleClientFromJson(Map<String, dynamic> json) => SimpleClient(
      id: json['id'] as int,
      phone: json['phone'] as String,
      name: json['name'] as String,
      date: json['date_create'] as String,
      image: json['photo'] as String,
      branch: json['branch'] as String,
    );

Map<String, dynamic> _$SimpleClientToJson(SimpleClient instance) =>
    <String, dynamic>{
      'id': instance.id,
      'phone': instance.phone,
      'name': instance.name,
      'photo': instance.image,
      'date_create': instance.date,
      'branch': instance.branch,
    };
