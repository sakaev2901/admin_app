// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'client.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Client _$ClientFromJson(Map<String, dynamic> json) => Client(
      phone: json['phone'] as String?,
      id: json['id'] as int,
      name: json['name'] as String?,
      branch: json['branch'] as int,
      email: json['email'] as String?,
      docNumber: json['doc_number'] as String?,
      dateBirth: json['date_birth'] as String?,
      docSeries: json['doc_series'] as String?,
      passportPhotos: (json['passport_photos'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$ClientToJson(Client instance) => <String, dynamic>{
      'id': instance.id,
      'phone': instance.phone,
      'name': instance.name,
      'email': instance.email,
      'date_birth': instance.dateBirth,
      'doc_series': instance.docSeries,
      'doc_number': instance.docNumber,
      'passport_photos': instance.passportPhotos,
      'branch': instance.branch,
    };
