import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'simple_client.g.dart';

@JsonSerializable()
class SimpleClient extends Equatable{
  final int id;
  final String phone;
  final String name;
  @JsonKey(name: "photo")
  final String image;
  @JsonKey(name: "date_create")
  final String date;
  final String branch;

  SimpleClient({required this.id, required this.phone, required this.name, required this.date, required this.image, required this.branch});

  factory SimpleClient.fromJson(Map<String, dynamic> json) => _$SimpleClientFromJson(json);

  @override
  List<Object?> get props => [id, phone, name, date, branch, image];

}
