import 'package:admin_app/bloc/client_bonus_info/client_bonus_info_bloc.dart';
import 'package:admin_app/bloc/validation/validation_bloc.dart';
import 'package:admin_app/config/custom_interceptor.dart';
import 'package:admin_app/config/keys.dart';
import 'package:admin_app/page/add_bonuses/add_bonuses_page.dart';
import 'package:admin_app/page/login/login_page.dart';
import 'package:admin_app/provider/client_provider.dart';
import 'package:admin_app/provider/login_provider.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'bloc/client_list/client_list_bloc.dart';
import 'bloc/login/login_bloc.dart';
import 'config/bloc_observer.dart';

void main() async {
  await Hive.initFlutter();
  var dio = Dio(BaseOptions(
    baseUrl: "https://backend-stroika-arenda.ru/api",
    connectTimeout: 5000,
    receiveTimeout: 5000,
  ));
  dio.interceptors.add(CustomInterceptors());
  GetIt.I.registerSingleton(dio);
  BlocOverrides.runZoned(() => runApp(const MyApp()),
      blocObserver: MainObserver());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => ClientBonusInfoBloc(),
        ),
        BlocProvider(
          create: (context) => LoginBloc(LoginProvider()),
        ),
        BlocProvider(
          create: (context) => ClientListBloc(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        builder: BotToastInit(),
        navigatorKey: Keys.navigatorKey,
        navigatorObservers: [BotToastNavigatorObserver()],
        theme: ThemeData(
          primarySwatch: Colors.blue,
          primaryColor: Color(0xFFFFDD55),
          accentColor: Color(0xFFF1F1F1),
          hintColor: Color(0xFF6F7481),
          dividerColor: Color(0xFFF2F2F2),
          canvasColor: Colors.transparent,
          scaffoldBackgroundColor: Colors.white,
          textTheme: TextTheme(
            bodyText2: TextStyle(
              color: Color(0xFF1C1F27),
              height: 1,
            ),
          ),
        ),
        home: LoginPage(),
      ),
    );
  }
}
