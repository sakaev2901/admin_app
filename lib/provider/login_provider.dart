import 'package:admin_app/service/login_service.dart';
import 'package:hive/hive.dart';

import '../service/login_service.dart';

class LoginProvider {

  late LoginService _loginService;

  LoginProvider() {
    _loginService = LoginService();
  }

  Future login(String phone, String password) async {
    phone = '+79173936213';
    password = '!e!3Sp?p';
    String token = await _loginService.login(_parsePhone(phone), password);
    var box = await Hive.openBox('box');
    await box.put('token', token);
  }

  String _parsePhone(String phone) {
    if (phone.isNotEmpty) {
      return phone.substring(1).replaceAll(' ', '');
    }
    return "";
  }

}