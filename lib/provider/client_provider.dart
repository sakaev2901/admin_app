import 'package:admin_app/model/simple_client.dart';
import 'package:admin_app/service/client_service.dart';

import '../model/client.dart';

class ClientProvider {

  final ClientService clientService = ClientService();

  Future<List<SimpleClient>> fetchClients() async {
    var response = await clientService.fetchClients();
    List<dynamic> maps = response.data['data'];
    return maps.map((e) => SimpleClient.fromJson(e)).toList();
  }

  Future<Client> fetchSingleClient(int id) async {
    var response = await clientService.fetchSingleClient(id);
    return Client.fromJson(response.data['data']);
  }

  Future<void> submitClient(int id) async {
    await clientService.validateClient(id);
  }
}