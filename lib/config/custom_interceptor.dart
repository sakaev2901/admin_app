import 'dart:convert';

import 'package:dio/dio.dart';
class CustomInterceptors extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    print('REQUEST[${options.method}] => PATH: ${options.path}');
    print(options.data);
    return super.onRequest(options, handler);
  }
  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    print('RESPONSE[${response.statusCode}] => PATH: ${response.requestOptions.path}');
    JsonEncoder encoder = new JsonEncoder.withIndent('  ');
    if (response.data['data'] != null) {
      response.data['data'] = decode(response.data['data']);
      String prettyprint = encoder.convert(response.data);
      print(prettyprint);
    }

    return super.onResponse(response, handler);
  }
  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    print('ERROR[${err.response?.statusCode}] => PATH: ${err.requestOptions.path}');
    return super.onError(err, handler);
  }

  dynamic decode(String data) {
    Codec<String, String> stringToBase64Url = utf8.fuse(base64Url);
    return jsonDecode(stringToBase64Url.decode(data));
  }
}