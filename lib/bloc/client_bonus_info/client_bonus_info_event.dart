part of 'client_bonus_info_bloc.dart';

abstract class ClientBonusInfoEvent extends Equatable {
  const ClientBonusInfoEvent();

  @override
  List<Object?> get props => [];
}

class ClientBonusInfoLoaded extends ClientBonusInfoEvent {
  final String phone;

  ClientBonusInfoLoaded(this.phone);

  @override
  List<Object?> get props => [phone];
}
