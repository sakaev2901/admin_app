import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'client_bonus_info_event.dart';
part 'client_bonus_info_state.dart';

class ClientBonusInfoBloc extends Bloc<ClientBonusInfoEvent, ClientBonusInfoState> {
  ClientBonusInfoBloc() : super(ClientBonusInfoInitial()) {
    on<ClientBonusInfoLoaded>(_load);
  }

  void _load(ClientBonusInfoLoaded event, Emitter<ClientBonusInfoState> emit) async {
    try {
      print('HELLO');
      emit(ClientBonusInfoLoadingInProgress());
      await Future.delayed(Duration(seconds: 3));
      emit(ClientBonusInfoLoadedSuccess());
    } catch (e) {
      emit(ClientBonusInfoLoadedSuccess());
    }
  }
}
