part of 'client_bonus_info_bloc.dart';

abstract class ClientBonusInfoState extends Equatable {
  const ClientBonusInfoState();

  @override
  List<Object> get props => [];
}

class ClientBonusInfoInitial extends ClientBonusInfoState {}

class ClientBonusInfoLoadingInProgress extends ClientBonusInfoState {}

class ClientBonusInfoLoadedFail extends ClientBonusInfoState {}

class ClientBonusInfoLoadedSuccess extends ClientBonusInfoState {}