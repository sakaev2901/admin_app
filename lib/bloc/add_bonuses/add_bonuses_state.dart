part of 'add_bonuses_bloc.dart';

abstract class AddBonusesState extends Equatable {
  const AddBonusesState();
}

class AddBonusesInitial extends AddBonusesState {
  @override
  List<Object> get props => [];
}
