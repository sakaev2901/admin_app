part of 'add_bonuses_bloc.dart';

abstract class AddBonusesEvent extends Equatable {
  const AddBonusesEvent();

  @override
  List<Object?> get props => [];
}

class AddBonusesLoaded extends AddBonusesEvent {

}
