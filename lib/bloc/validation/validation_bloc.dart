import 'dart:async';

import 'package:admin_app/bloc/client_list/client_list_bloc.dart';
import 'package:admin_app/config/keys.dart';
import 'package:admin_app/provider/client_provider.dart';
import 'package:bloc/bloc.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:equatable/equatable.dart';

part 'validation_event.dart';
part 'validation_state.dart';

class ValidationBloc extends Bloc<ValidationEvent, ValidationState> {
  final ClientProvider clientProvider;
  final ClientListBloc clientListBloc;

  ValidationBloc(this.clientProvider, this.clientListBloc) : super(ValidationInitial()) {
    on<ValidationSubmitted>(_submit);
  }

  void _submit(ValidationSubmitted event, Emitter<ValidationState> emit) async {
    try {
      BotToast.showLoading();
      await clientProvider.submitClient(event.id);
      clientListBloc.add(ClientListLoaded());
      Keys.navigatorKey.currentState?.pop();
      emit(ValidationSuccess());
      print('eeeeeeeeee');
    } catch(e) {
      print(e);
      emit(ValidationFail());
    } finally {
      BotToast.closeAllLoading();
    }
  }


}
