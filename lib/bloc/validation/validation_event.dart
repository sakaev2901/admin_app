part of 'validation_bloc.dart';

abstract class ValidationEvent extends Equatable {
  const ValidationEvent();

  @override
  List<Object?> get props => [];
}

class ValidationSubmitted extends ValidationEvent {
  final int id;

  ValidationSubmitted(this.id);

  @override
  List<Object?> get props => [id];

}
