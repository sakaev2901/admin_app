part of 'client_bloc.dart';

abstract class ClientEvent extends Equatable {
  const ClientEvent();

  @override
  List<Object?> get props => [];
}

class ClientLoaded extends ClientEvent {
  final int id;

  ClientLoaded(this.id);

  @override
  List<Object?> get props => [id];
}
