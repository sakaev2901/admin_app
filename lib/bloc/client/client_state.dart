part of 'client_bloc.dart';

abstract class ClientState extends Equatable {
  const ClientState();

  @override
  List<Object> get props => [];
}



class ClientLoadedSuccess extends ClientState {
  final Client client;

  ClientLoadedSuccess(this.client);

  @override
  List<Object> get props => [client.name ?? '-'];

}

class ClientLoadedFail extends ClientState {}

class ClientInitial extends ClientState {}