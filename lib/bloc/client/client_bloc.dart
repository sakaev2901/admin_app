import 'dart:async';

import 'package:admin_app/provider/client_provider.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../model/client.dart';

part 'client_event.dart';
part 'client_state.dart';

class ClientBloc extends Bloc<ClientEvent, ClientState> {
  final ClientProvider clientProvider;

  ClientBloc(this.clientProvider) : super(ClientInitial()) {
    on<ClientLoaded>(_loadClient);
  }

  void _loadClient(ClientLoaded event, Emitter<ClientState> emit) async {
    try {
      emit(ClientLoadedSuccess(await clientProvider.fetchSingleClient(event.id)));
    } catch (e) {
      print(e);
      emit(ClientLoadedFail());
    }
  }
}
