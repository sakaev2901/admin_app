part of 'account_recovery_bloc.dart';

abstract class AccountRecoveryEvent extends Equatable {
  const AccountRecoveryEvent();

  @override
  List<Object?> get props => [];
}

class AccountRecoveryButtonPressed extends AccountRecoveryEvent {
  final String phone;

  AccountRecoveryButtonPressed(this.phone);

  @override
  List<Object?> get props => [phone];
}
