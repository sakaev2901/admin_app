import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../errors/base_exception.dart';
import '../../provider/profile_provider.dart';

part 'account_recovery_event.dart';

part 'account_recovery_state.dart';

class AccountRecoveryBloc
    extends Bloc<AccountRecoveryEvent, AccountRecoveryState> {
  final ProfileProvider profileProvider;

  AccountRecoveryBloc(this.profileProvider) : super(AccountRecoveryInitial()) {
    on<AccountRecoveryButtonPressed>(_recoveryAccount);
  }

  void _recoveryAccount(AccountRecoveryButtonPressed event,
      Emitter<AccountRecoveryState> emit) async {
    try {
      await profileProvider.recoveryAccount(event.phone);
      emit(AccountRecoveryLoadedSuccess());
    } on BaseException catch (e) {
      emit(AccountRecoveryLoadedFail(error: e.cause));
    } catch (e) {
      print(e.toString());
      emit(AccountRecoveryLoadedFail());
    }
  }
}
