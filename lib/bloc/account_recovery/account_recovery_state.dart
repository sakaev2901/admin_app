part of 'account_recovery_bloc.dart';

abstract class AccountRecoveryState extends Equatable {
  const AccountRecoveryState();

  @override
  List<Object> get props => [];
}

class AccountRecoveryInitial extends AccountRecoveryState {}

class AccountRecoveryLoadedSuccess extends AccountRecoveryState {}

class AccountRecoveryLoadedFail extends AccountRecoveryState {
  String? error;

  AccountRecoveryLoadedFail({this.error});

  @override
  List<Object> get props => [error ?? ''];
}
