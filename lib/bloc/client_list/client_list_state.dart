part of 'client_list_bloc.dart';

abstract class ClientListState extends Equatable {
  const ClientListState();

  @override
  List<Object> get props => [];
}

class ClientListInitial extends ClientListState {}

class ClientListProgressState extends ClientListState {}

class ClientListSuccessState extends ClientListState {
  final List<SimpleClient> clients;

  ClientListSuccessState(this.clients);

  @override
  List<Object> get props => [clients];

}

class ClientListFailState extends ClientListState {
  final String error;

  ClientListFailState(this.error);

  @override
  List<Object> get props => [error];
}
