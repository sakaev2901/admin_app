part of 'client_list_bloc.dart';

abstract class ClientListEvent extends Equatable {
  const ClientListEvent();

  @override
  List<Object?> get props => [];
}

class ClientListLoaded extends ClientListEvent {}
