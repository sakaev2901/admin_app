import 'dart:async';

import 'package:admin_app/model/simple_client.dart';
import 'package:admin_app/provider/client_provider.dart';
import 'package:bloc/bloc.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:equatable/equatable.dart';

part 'client_list_event.dart';
part 'client_list_state.dart';

class ClientListBloc extends Bloc<ClientListEvent, ClientListState> {

  final ClientProvider clientProvider = ClientProvider();

  ClientListBloc() : super(ClientListInitial()) {
    on<ClientListLoaded>(_fetchClients);
  }

  void _fetchClients(ClientListLoaded event, Emitter<ClientListState> emitter) async {
    try {
      BotToast.showLoading();
      emitter(ClientListSuccessState(await clientProvider.fetchClients()));
    } catch (e) {
      emitter(ClientListFailState(e.toString()));
    } finally {
      BotToast.closeAllLoading();
    }
  }
}
