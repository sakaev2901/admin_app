part of 'login_bloc.dart';

abstract class LoginState extends Equatable {
  const LoginState();

  @override
  List<Object> get props => [];
}

class LoginInitial extends LoginState {}

class LoginSuccessState extends LoginState {}

class LoginProgressState extends LoginState {}

class LoginFailState extends LoginState {
  final String error;

  LoginFailState(this.error);
}

