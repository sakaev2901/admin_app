import 'dart:async';

import 'package:admin_app/provider/login_provider.dart';
import 'package:bloc/bloc.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:equatable/equatable.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {

  final LoginProvider loginProvider;

  LoginBloc(this.loginProvider) : super(LoginInitial()) {
    on<LoginButtonPressed>(_login);
  }

  void _login(LoginButtonPressed event, Emitter<LoginState> emitter) async {
    try {
      BotToast.showLoading();
      emitter(LoginProgressState());
      await loginProvider.login(event.phone, event.password);
      emitter(LoginSuccessState());
    } catch (e) {
      emitter(LoginFailState(e.toString()));
    } finally {
      BotToast.closeAllLoading();
    }
  }

}
